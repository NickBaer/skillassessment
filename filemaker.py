#!/usr/bin/env python3

"""
just a simple file maker for testing purposes
"""

from typing import TextIO


def makefile(lines: int = 2019):
    """
    Creates a txt-file with given number of lines
    :param lines: Number of lines
    :return: None
    """
    f: TextIO = open("testfile.txt", "w+")
    for i in range(lines):
        f.write("Line %d\n" % (i+1))
    f.close()


if __name__ == "__main__":
    makefile()
