#!/usr/bin/env python3

"""
Splits a file into multiple files, each with the given number of lines
usage: pysplit.py [-h] [-l LINES] [-v] file
"""

import argparse
import os
import sys
import ntpath
from typing import TextIO


def split(file: str, lines: int = 1000, verbose: bool = False) -> None:
    """
    Entry-Method
    Given parameters are checked for validity before they are given to _filesplitter(...)
    :return: None
    """

    # validity check of given filename
    if not os.path.isfile(file):
        print(f'File "{file}" not found')
        sys.exit(3)
    if verbose:
        print(f'File is "{file}"')

    # validity check of given line number
    if lines <= 0:
        print(f'Lines must be greater than 0, ("{lines}" given)')
        sys.exit(4)
    if verbose:
        print(f'Lines is "{lines}"')

    # checking writing access to directory
    path, filename = ntpath.split(file)
    if path == "":
        path = os.getcwd()
    if not os.access(path, os.W_OK):
        print(f'"{path}" is not writeable')
        sys.exit(5)

    _filesplitter(file, lines, verbose)


def _filesplitter(orig_file: str, lines: int, verbose: bool) -> None:
    """
    Splits a file into multiple files with given number of lines.
    Original file will not be altered. New files will have consecutive numbers attached to their filename
    :param orig_file: Path to original files
    :param lines: Number of lines in new files
    :param verbose: verbose of true
    :return: None
    """

    # Getting needed file information
    path, filename = os.path.split(orig_file)
    basename, ext = os.path.splitext(filename)
    # number of first file
    filenumber = 1

    # Opening original file to read
    with open(orig_file, 'r') as org_f:
        try:
            # Reading file linewise and writing lines in new file
            out_file: TextIO = open(os.path.join(path, '{}{}{}'.format(basename, filenumber, ext)), 'w')
            for i, line in enumerate(org_f):
                if i % lines == 0:
                    # Whenever number of written lines is a multiple of wanted lines per file the file is closed and
                    # a new one os opened
                    out_file.close()
                    out_file: TextIO = open(os.path.join(path, '{}{}{}'.format(basename, filenumber, ext)), 'w')
                    if verbose:
                        print(f'Writing {filenumber}. file')
                    filenumber += 1
                out_file.write(line)
        finally:
            # cleaning up
            out_file.close()
            if verbose:
                print("All files written")


if __name__ == "__main__":
    """
    Handling of Command line parameters if script is run from command line
    """
    # Construction of parameter parser
    parser = argparse.ArgumentParser(
        description='Splits a file into multiple files, each with the given number of lines (the last one may have '
                    'less). New files have New files will have consecutive numbers attached to their filename. The '
                    'original file will not be altered.')
    parser.add_argument("file", help="The file that will be split")
    parser.add_argument("-l", "--lines", type=int, default=1000,
                        help="Number of lines that each output file will have \nDefault: 1000")
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")

    # Parsing parameters
    args = parser.parse_args()

    # running split(...)
    split(args.file, args.lines, args.verbose)
